<% local data, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% viewlibrary.dispatch_component("status") %>

<%
local header_level = htmlviewfunctions.displaysectionstart(data, page_info)
local header_level2 = htmlviewfunctions.incrementheader(header_level)
if not data.value.show_isakmp or #data.value.show_isakmp.value == 0 then
	io.write("<p>No status details available</p>")
else
	for i,entry in pairs(data.value.show_isakmp.value) do
		if (type(entry) ==  "table") and (entry.Destination) and (entry.Destination.value) and (#entry.Destination.value > 0) then
			htmlviewfunctions.displaysectionstart(cfe({label=entry.Destination.value}), page_info, header_level2)
			io.write("<table>\n")
			local tags = {"Created","Source","Destination", "St", "Phase2details"}
			for j,tag in pairs(tags) do
				io.write("<tr><td style='font-weight:bold;width:120px;border:none;'>" ..
					html.html_escape(entry[tag].label) .. "</td><td style='border:none;'>"..string.gsub(html.html_escape(entry[tag].value), "\n", "<br/>"))
				if (entry[tag].descr) and (#entry[tag].descr > 0) then io.write(" (".. html.html_escape(entry[tag].descr) .. ")") end
				io.write("</td></tr>")
			end
			io.write("</table>")
			htmlviewfunctions.displaysectionend(header_level2)
		end
	end
end
--htmlviewfunctions.displayitem(data.value.ip_xfrm_policy)
htmlviewfunctions.displaysectionend(header_level)
%>
