<% local data, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>

<% htmlviewfunctions.displaycommandresults({"editracoon", "editipsec"}, session, true) %>

<%
viewlibrary.dispatch_component("status")
viewlibrary.dispatch_component("editracoon")
viewlibrary.dispatch_component("editipsec")
%>
