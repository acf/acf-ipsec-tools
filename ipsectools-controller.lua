local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.details(self)
	return self.model.getstatusdetails()
end

function mymodule.editracoon (self)
	return self.handle_form(self, self.model.get_racoonfiledetails, self.model.update_racoonfiledetails, self.clientdata, "Save", "Edit Racoon Config", "Configuration Set")
end

function mymodule.editipsec (self)
	return self.handle_form(self, self.model.get_ipsecfiledetails, self.model.update_ipsecfiledetails, self.clientdata, "Save", "Edit IPSec Config", "Configuration Set")
end

function mymodule.listcerts(self)
	return self.model.list_certs()
end

function mymodule.deletecert(self)
	return self.handle_form(self, self.model.get_delete_cert, self.model.delete_cert, self.clientdata, "Delete", "Delete Certificate", "Certificate Deleted")
end

function mymodule.uploadcert (self)
	return self.handle_form(self, self.model.new_upload_cert, self.model.upload_cert, self.clientdata, "Upload", "Upload Certificate", "Certificate Uploaded")
end

function mymodule.viewcert(self)
	return self.handle_form(self, self.model.get_view_cert, self.model.view_cert, self.clientdata, "View", "View Certificate")
end

function mymodule.logfile(self)
	return self.model.get_logfile(self, self.clientdata)
end

return mymodule
