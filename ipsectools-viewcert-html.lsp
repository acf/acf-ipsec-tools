<% local view, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<% if view.value.result then %>
	<% local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Certificate Details"}), page_info) %>
	<% htmlviewfunctions.displayitem(view.value.cert, page_info) %>
	<pre><%= html.html_escape(view.value.result.value) %></pre>
	<% htmlviewfunctions.displaysectionend(header_level) %>
<% else
	htmlviewfunctions.displayitem(view, page_info)
end %>
