<% local view, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"deletecert"}, session) %>
<% htmlviewfunctions.displaycommandresults({"uploadcert"}, session, true) %>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<table id="list" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>Certificate</th>
	</tr>
</thead><tbody>
<% local certcfe = cfe({ type="hidden", value="" }) %>
<% for i,cert in ipairs(view.value) do %>
	<tr>
		<td>
		<% certcfe.value = cert %>
		<% htmlviewfunctions.displayitem(cfe({type="form", value={cert=certcfe}, label="", option="Delete", action="deletecert" }), page_info, -1) %>
		<% if not string.find(cert, "%-key") then %>
		<% htmlviewfunctions.displayitem(cfe({type="link", value={cert=certcfe}, label="", option="View", action="viewcert" }), page_info, -1) %>
		<% end %>
		</td>
		<td><%= html.html_escape(cert) %></td>
	</tr>
<% end %>
</tbody></table>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% if viewlibrary.dispatch_component and viewlibrary.check_permission("uploadcert") then
	viewlibrary.dispatch_component("uploadcert")
end %>
